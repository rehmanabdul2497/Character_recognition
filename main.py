from keras.models import Sequential, load_model
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.preprocessing.image import  img_to_array
from keras import backend as K
import numpy as np
import os
from PIL import Image
from sklearn.cross_validation import train_test_split
from keras.optimizers import Adam, SGD
from keras.layers.normalization import BatchNormalization
from keras.callbacks import ModelCheckpoint

m,n = 80,80
classes=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
model = load_model('/Users/apple/Desktop/FINAL ML/CNN_character_recognition.h5')
inp = input("\n")
while inp!='exit':
	im = Image.open("/Users/apple/Desktop/FINAL ML/test"+inp)
	im=im.convert(mode='1')
	imrs = im.resize((m,n))
	imrs=img_to_array(imrs)
	imrs=imrs.transpose(2,0,1)
	imrs=imrs.reshape(m,n,1)
	x=[]
	x.append(imrs)
	x=np.array(x)
	predictions = model.predict(x)
	alpha = predictions[0].argmax(axis=0)
	print(classes[alpha])
	inp = input("\n")



# x=np.array(x);
# predictions = model.predict(x)
# print(predictions)
# alpha = predictions[0].argmax(axis=0)
# print(classes[alpha])
