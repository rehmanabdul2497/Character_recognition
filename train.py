from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.preprocessing.image import  img_to_array
from keras import backend as K
import numpy as np
import os
from PIL import Image
from sklearn.cross_validation import train_test_split
from keras.optimizers import Adam, SGD
from keras.layers.normalization import BatchNormalization
from keras.callbacks import ModelCheckpoint
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix

m,n = 80,80

path2="/Users/apple/Desktop/FINAL ML/training";

classes=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
print(classes)
x=[]
y=[]
for fol in classes:
    print(fol)
    imgfiles=os.listdir(path2+'/'+fol);
    for img in imgfiles:
        if (img.startswith("Thumb") or img.startswith(".")):
            continue
        im=Image.open(path2+'/'+fol+'/'+img);
        im=im.convert(mode='1')
        imrs=im.resize((m,n))
        imrs=img_to_array(imrs);
        imrs=imrs.transpose(2,0,1);
        imrs=imrs.reshape(m,n,1);
        x.append(imrs);
        y.append(fol);

x=np.array(x);
y=np.array(y);

print(x.shape)
print(y.shape)

batch_size=32
nb_classes=len(classes)
print(nb_classes)

x_train, x_test, y_train, y_test= train_test_split(x,y,test_size=0.2,random_state=4)

uniques, id_train=np.unique(y_train,return_inverse=True)
Y_train=np_utils.to_categorical(id_train,nb_classes)
uniques, id_test=np.unique(y_test,return_inverse=True)
Y_test=np_utils.to_categorical(id_test,nb_classes)

# print("x_train = ",x_train[0].shape)
# print(y_train.shape)
# print(x_test.shape)
# print(y_test.shape)

# print(type(x_train))
# print(type(x_train[0]))
# print(type(x_train[0][0]))
# print(type(x_train[0][0][0]))

model = Sequential()
model.add(Conv2D(32, (5, 5), activation='relu', padding='same', input_shape=x_train.shape[1:]))
model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))

model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))

model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())

model.add(Dense(2048, activation='relu')) 

model.add(Dropout(0.5))

model.add(Dense(nb_classes, activation='softmax'))

model.summary()

nb_epoch=20;
# learning_rate = 0.1
# decay_rate = learning_rate / nb_epoch
# momentum = 0.8
# sgd = SGD(lr=learning_rate, momentum=momentum, decay=decay_rate, nesterov=False)
batch_size=128;
model.compile(loss = 'categorical_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
history = model.fit(x_train,Y_train,batch_size=batch_size,epochs=nb_epoch,verbose=1,validation_data=(x_test, Y_test))

model.save("/Users/apple/Desktop/FINAL ML/CNN_character_recognition.h5")

scores = model.evaluate(x_test, Y_test, verbose=0)
print('Test Accuracy : %.2f%%' % scores[1])

plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

preds = model.predict(x_test)
predicts = np.argmax(preds, axis = 1)
y = np.argmax(Y_test, axis =1)
cm = confusion_matrix(y, predicts)
print(cm)

print('Actual_labels  |  Predicted_labels')
for i in range(20):
    print('      %3d      |     %3d' %(y[i], predicts[i]))