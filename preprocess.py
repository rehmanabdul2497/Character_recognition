import os
from PIL import Image
import PIL.ImageOps

path2="/Users/apple/Desktop/training";

classes=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
print(classes)
for fol in classes:
    print(fol)
    imgfiles=os.listdir(path2+'/'+fol);
    for img in imgfiles:
        if (img.startswith("Thumb") or img.startswith(".")):
            continue
        im=Image.open(path2+'/'+fol+'/'+img);
        im=im.convert(mode='RGB')
        im=PIL.ImageOps.invert(im)
        im=im.convert(mode='1')
        im.save(path2+'/'+fol+'/'+'100'+img)